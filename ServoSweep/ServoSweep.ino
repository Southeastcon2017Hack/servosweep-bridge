/* Sweep
 by BARRAGAN <http://barraganstudio.com> 
 This example code is in the public domain.

 modified 28 May 2015
 by Michael C. Miller
 modified 8 Nov 2013
 by Scott Fitzgerald
modified 27 Mar 2017
by Andrew Elek

 http://arduino.cc/en/Tutorial/Sweep
*/ 

#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // twelve servo objects can be created on most boards
Servo myservo2; 

void setup() 
{ 
  Serial.begin (9600);
  myservo.attach(D2);  // attaches the servo on GIO2 to the servo object 
  myservo2.attach(D6);
} 
 
void loop() 
{ 
  int pos;

  for(pos = 0; pos <= 90; pos += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    myservo2.write(abs(pos-180));
    delay(25);                       // waits 15ms for the servo to reach the position 
  } 
    delay(4000);
  for(pos = 90; pos>=0; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    myservo2.write(abs(180-pos));
    delay(25);                       // waits 15ms for the servo to reach the position 
  } 
  delay(4000);
} 

